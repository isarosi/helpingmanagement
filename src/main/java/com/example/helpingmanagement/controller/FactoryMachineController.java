package com.example.helpingmanagement.controller;

import com.example.helpingmanagement.dto.FactoryMachineDTO;
import com.example.helpingmanagement.entities.MachineType;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.service.FactoryMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@Controller
public class FactoryMachineController {

    @Autowired
    FactoryMachineService factoryMachineService;

    @RequestMapping(value = "/factoryMachinesHome")
    public String factoryMachinesHome(){
        return "factoryMachines";
    }

    @RequestMapping(value = "/getFactoryMachines")
    public List<FactoryMachineDTO> getFactoryMachines() {
        return factoryMachineService.getAllFactoryMachines();
    }

    @RequestMapping(value = "/listOfAllFactoryMachines")
    public String listOfAllFactoryMachines (Model model) {
        List<FactoryMachineDTO> factoryMachineList = factoryMachineService.getAllFactoryMachines();
        model.addAttribute("factoryMachineList", factoryMachineList);
        return "factoryMachineList";
    }

    @RequestMapping(value = "/getFactoryMachine/{id}")
    public String factoryMachineById(@PathVariable Long id, Model model) throws NoSuchEntityException {
        FactoryMachineDTO factoryMachineDTO = factoryMachineService.findById(id);
        model.addAttribute("factoryMachineById", factoryMachineDTO);
        return "factoryMachineById";
    }

    @GetMapping(value = "/searchById")
    public String requestId(@RequestParam(value = "idInput") Integer id, Model model) throws NoSuchEntityException {
        List<FactoryMachineDTO> factoryMachineDTOList = factoryMachineService.searchByAnId(id);
        model.addAttribute("factoryMachineById", factoryMachineDTOList);
        return "factoryMachineById";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/factoryMachine")
    public String addFactoryMachine(@ModelAttribute FactoryMachineDTO factoryMachineDTO) throws ParseException {
        factoryMachineService.addFactoryMachine(factoryMachineDTO);
        return "factoryMachineAddedResponse";
    }

    @RequestMapping(value = "/createFactoryMachinePage")
    public String createFactoryMachinePage (Model model) {
        FactoryMachineDTO factoryMachineDTO = new FactoryMachineDTO();
        model.addAttribute("createFactoryMachine", factoryMachineDTO);
        return "factoryMachineAdding";
    }

    @PostMapping(value = "/updateFactoryMachine/{id}")
    public String updateFactoryMachine(@ModelAttribute FactoryMachineDTO factoryMachineDTO) throws NoSuchEntityException, ParseException {
        factoryMachineService.updateFactoryMachine(factoryMachineDTO);
        return "factoryMachineUpdatedResponse";
    }

    @GetMapping("/updateFactoryMachine/{id}")
    public String updateFactoryMachinePage(@PathVariable("id") Long id, Model model) {
        FactoryMachineDTO factoryMachineDTO = null;
        try {
            factoryMachineDTO = factoryMachineService.findById(id);
        } catch (NoSuchEntityException e) {
            e.printStackTrace();
        }
        model.addAttribute("updateFactoryMachine", factoryMachineDTO);
        return "factoryMachineUpdate";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteFactoryMachine/{id}")
    public List<FactoryMachineDTO> deleteFactoryMachine(@PathVariable Long id) {
        factoryMachineService.deleteFactoryMachine(id);
        return factoryMachineService.getAllFactoryMachines();
    }

    @PostMapping(value = "/deleteFactoryMachine/{id}")
    public String deleteFactoryMachinePage(@PathVariable Long id) {
        factoryMachineService.deleteFactoryMachine(id);
        return "factoryMachineDeletedResponse";
    }
}
