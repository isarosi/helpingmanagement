package com.example.helpingmanagement.controller;

import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.service.EmployeeService;
import com.example.helpingmanagement.service.FactoryMachineService;
import com.example.helpingmanagement.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TimeLineController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    FactoryMachineService factoryMachineService;

    @Autowired
    VehicleService vehicleService;

    @GetMapping(value = "/timeline")
    public String timelinePage(@RequestParam(value = "noOfDays") int noOfDays, Model model) {
        List<TimelineDTO> timelineDTOList = new ArrayList<>();
        timelineDTOList.addAll(employeeService.getEmployeesWithCloseRaiseDate(noOfDays));
        timelineDTOList.addAll(factoryMachineService.getFactoryMachinesWithToDoDate(noOfDays));
        timelineDTOList.addAll(vehicleService.getVehiclesWithToDoDate(noOfDays));
        model.addAttribute("timelineList", timelineDTOList);
        return "timeline";
    }
}
