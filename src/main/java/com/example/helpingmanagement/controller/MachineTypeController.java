package com.example.helpingmanagement.controller;


import com.example.helpingmanagement.dto.MachineTypeDTO;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.service.MachineTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MachineTypeController {

    @Autowired
    MachineTypeService machineTypeService;

    @RequestMapping(value = "/machineTypesHome")
    public String machineTypesHome(){
        return "machineTypes";
    }

    @RequestMapping(value = "/getMachineTypes")
    public List<MachineTypeDTO> getMachineTypes () {
        return machineTypeService.getAllMachineTypes();
    }

    @RequestMapping(value = "/listOfAllMachineTypes")
    public String listOfAllMachineTypes (Model model) {
        List<MachineTypeDTO> machineTypeDTOList = machineTypeService.getAllMachineTypes();
        model.addAttribute("machineTypeList", machineTypeDTOList);
        return "machineTypeList";
    }

    @RequestMapping("/seeMachineType/{id}")
    public MachineTypeDTO getMachineType(@PathVariable Long id) throws NoSuchEntityException {
        return machineTypeService.findById(id);
    }

    @RequestMapping(value = "/getMachineType/{id}")
    public String machineTypeById(@PathVariable Long id, Model model) throws NoSuchEntityException {
        MachineTypeDTO machineTypeDTO = machineTypeService.findById(id);
        model.addAttribute("machineTypeById", machineTypeDTO);
        return "machineTypeById";
    }

    @PostMapping(value = "/machineType")
    public String addMachineType(@ModelAttribute MachineTypeDTO machineType) {
        machineTypeService.addMachineType(machineType);
        return "machineTypeAddedResponse";
    }

    @RequestMapping(value = "/createMachineType")
    public String createMachineTypePage (Model model) {
        MachineTypeDTO machineTypeDTO = new MachineTypeDTO();
        model.addAttribute("machineTypeDTO", machineTypeDTO);
        return "machineTypeAdding";
    }

    @PostMapping(value = "/updateMachineType/{id}")
    public String updateMachineType(@ModelAttribute MachineTypeDTO machineTypeDTO) throws NoSuchEntityException {
        machineTypeService.updateMachineType(machineTypeDTO);
        return "machineTypeUpdatedResponse";
    }

    @GetMapping("/updateMachineType/{id}")
    public String updateMachineTypePage(@PathVariable("id") Long id, Model model) {
        MachineTypeDTO machineTypeDTO = null;
        try {
            machineTypeDTO = machineTypeService.findById(id);
        } catch (NoSuchEntityException e) {
            e.printStackTrace();
        }
        model.addAttribute("updateMachineType", machineTypeDTO);
        return "machineTypeUpdate";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteMachineType/{id}")
    public List<MachineTypeDTO> deleteMachineType(@PathVariable Long id) {
        machineTypeService.deleteMachineType(id);
        return machineTypeService.getAllMachineTypes();
    }

    @PostMapping(value = "/deleteMachineType/{id}")
    public String deleteMachineTypePage(@PathVariable Long id) {
        machineTypeService.deleteMachineType(id);
        return "machineTypeDeletedResponse";
    }
}
