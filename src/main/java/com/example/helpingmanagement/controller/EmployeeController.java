package com.example.helpingmanagement.controller;


import com.example.helpingmanagement.dto.EmployeeDTO;
import com.example.helpingmanagement.entities.Employee;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value = "/employeesHome")
    public String employeesHome() {
        return "employees";
    }

    @RequestMapping(value = "/getEmployees")
    public List<EmployeeDTO> getEmployees() {
        return employeeService.getAllEmployees();
    }

    @RequestMapping(value = "/listOfAllEmployees")
    public String listOfAllEmployeesPage(Model model) {
        List<EmployeeDTO> employeeDTOList = employeeService.getAllEmployees();
        model.addAttribute("employeeList", employeeDTOList);
        return "employeeList";
    }

    @RequestMapping("/seeEmployee/{id}")
    public EmployeeDTO getEmployee(@PathVariable Long id) throws NoSuchEntityException {
        return employeeService.findById(id);
    }

    @RequestMapping(value = "/getEmployee/{id}")
    public String employeeById(@PathVariable Long id, Model model) throws NoSuchEntityException {
        EmployeeDTO employeeDTO = employeeService.findById(id);
        model.addAttribute("employeeById", employeeDTO);
        return "employeeById";
    }

    @GetMapping(value = "/search")
    public String requestParam(@RequestParam(value = "searchInput") String lastName, Model model) throws NoSuchEntityException {
        List<EmployeeDTO> employeeDTOList = employeeService.findByFirstAndLastName(lastName);
        model.addAttribute("employeeById", employeeDTOList);
        return "employeeById";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/employee")
    public String addEmployee(@ModelAttribute EmployeeDTO employeeDTO) throws ParseException {
        employeeService.addEmployee(employeeDTO);
        return "employeeAddedResponse";
    }

    @RequestMapping(value = "/createEmployee")
    public String createEmployeePage(Model model) {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        model.addAttribute("createEmployee", employeeDTO);
        return "employeeAdding";
    }

    @PostMapping(value = "/updateEmployee/{id}")
    public String updateEmployee(@ModelAttribute EmployeeDTO employeeDTO) throws NoSuchEntityException, ParseException {
        employeeService.updateEmployee(employeeDTO);
        return "employeeUpdatedResponse";
    }

    @GetMapping("/updateEmployee/{id}")
    public String updateEmployeePage(@PathVariable("id") Long id, Model model) {
        EmployeeDTO employeeDTO = null;
        try {
            employeeDTO = employeeService.findById(id);
        } catch (NoSuchEntityException e) {
            e.printStackTrace();
        }
        model.addAttribute("updateEmployee", employeeDTO);
        return "employeeUpdate";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteEmployee/{id}")
    public List<EmployeeDTO> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return employeeService.getAllEmployees();
    }

    @PostMapping(value = "/deleteEmployee/{id}")
    public String deleteEmployeePage(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return "employeeDeletedResponse";
    }
}
