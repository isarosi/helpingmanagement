package com.example.helpingmanagement.controller;

import com.example.helpingmanagement.dto.VehicleDTO;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@Controller
public class VehicleController {

    @Autowired
    VehicleService vehicleService;

    @RequestMapping(value = "/vehiclesHome")
    public String vehiclesHome(){
        return "vehicles";
    }

    @RequestMapping(value = "/getVehicles")
    public List<VehicleDTO> getVehicles () {
        return vehicleService.getAllVehicles();
    }

    @RequestMapping(value = "/listOfAllVehicles")
    public String listOfAllVehicles (Model model) {
        List<VehicleDTO> vehicleDTOList = vehicleService.getAllVehicles();
        model.addAttribute("vehicleList", vehicleDTOList);
        return "vehicleList";
    }

    @RequestMapping("/vehicle/{registrationPlate}")
    public VehicleDTO getVehicle(@PathVariable String registrationPlate) throws NoSuchEntityException {
        return vehicleService.findByRegistrationPlate(registrationPlate);
    }

    @RequestMapping(value = "/getVehicle/{registrationPlate}")
    public String vehicleByRegistrationPlate(@PathVariable String registrationPlate, Model model) throws NoSuchEntityException {
        VehicleDTO vehicleDTO = vehicleService.findByRegistrationPlate(registrationPlate);
        model.addAttribute("vehicleByRegistrationPlate", vehicleDTO);
        return "vehicleByRegistrationPlate";
    }

    @GetMapping(value = "/searchByRegPate")
    public String requestRegPlate(@RequestParam(value = "regPlateInput") String regPlate, Model model) throws NoSuchEntityException {
        List<VehicleDTO> vehicleDTOList = vehicleService.findByRPlate(regPlate);
        model.addAttribute("vehicleByRegistrationPlate", vehicleDTOList);
        return "vehicleByRegistrationPlate";
    }

    @PostMapping(value = "/vehicle")
    public String addVehicle(@ModelAttribute VehicleDTO vehicleDTO) throws ParseException {
        vehicleService.addVehicle(vehicleDTO);
        return "vehicleAddedResponse";
    }

    @RequestMapping(value = "/createVehiclePage")
    public String createVehiclePage (Model model) {
        VehicleDTO vehicleDTO = new VehicleDTO();
        model.addAttribute("createVehicle", vehicleDTO);
        return "vehicleAdding";
    }

    @PostMapping(value = "/updateVehicle/{registrationPlate}")
    public String updateVehicle(@ModelAttribute VehicleDTO vehicleDTO) throws NoSuchEntityException, ParseException {
        vehicleService.updateVehicle(vehicleDTO);
        return "vehicleUpdatedResponse";
    }

    @GetMapping("/updateVehicle/{registrationPlate}")
    public String updateVehiclePage(@PathVariable("registrationPlate") String registrationPlate, Model model) {
        VehicleDTO vehicleDTO = null;
        try {
            vehicleDTO = vehicleService.findByRegistrationPlate(registrationPlate);
        } catch (NoSuchEntityException e) {
            e.printStackTrace();
        }
        model.addAttribute("updateVehicle", vehicleDTO);
        return "vehicleUpdate";
    }

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteVehicle/{registrationPlate}")
    public List<VehicleDTO> deleteVehicle(@PathVariable String registrationPlate) {
        vehicleService.deleteVehicle(registrationPlate);
        return vehicleService.getAllVehicles();
    }

    @Transactional
    @PostMapping(value = "/deleteVehicle/{registrationPlate}")
    public String deleteVehiclePage(@PathVariable String registrationPlate) {
        vehicleService.deleteVehicle(registrationPlate);
        return "vehicleDeletedResponse";
    }
}
