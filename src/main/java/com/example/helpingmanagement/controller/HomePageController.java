package com.example.helpingmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomePageController {

    @RequestMapping(value = "/homePage")
    public String homePage(){
        return "homePage";
    }
}
