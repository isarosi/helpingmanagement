package com.example.helpingmanagement;

import com.example.helpingmanagement.service.EmployeeService;
import com.example.helpingmanagement.service.EmployeeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpingManagementApplication {


    public static void main(String[] args) {
        SpringApplication.run(HelpingManagementApplication.class, args);


    }

}
