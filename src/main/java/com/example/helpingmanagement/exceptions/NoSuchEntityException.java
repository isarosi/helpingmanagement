package com.example.helpingmanagement.exceptions;

public class NoSuchEntityException extends Exception {

    public NoSuchEntityException(String id) {
        super("Id " + id + " is non-existent!");
    }
}
