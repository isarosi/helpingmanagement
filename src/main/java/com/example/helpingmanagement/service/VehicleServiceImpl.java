package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.dto.VehicleDTO;
import com.example.helpingmanagement.entities.Vehicle;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Override
    public List<VehicleDTO> getAllVehicles() {
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        List<VehicleDTO> list = new ArrayList<>();
        for (Vehicle vehicle : vehicleList) {
            VehicleDTO dto = createDTO(vehicle);
            list.add(dto);
        }
        return list;
    }

    private VehicleDTO createDTO(Vehicle vehicle) {
        VehicleDTO dto = new VehicleDTO();
        dto.setRegistrationPlate(vehicle.getRegistrationPlate());
        DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        String strDate1 = dateFormat1.format(vehicle.getOilAndFilters());
        dto.setOilAndFilters(strDate1);
        DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
        String strDate2 = dateFormat2.format(vehicle.getTimingBelt());
        dto.setTimingBelt(strDate2);
        DateFormat dateFormat3 = new SimpleDateFormat("MM/dd/yyyy");
        String strDate3 = dateFormat3.format(vehicle.getInsurance());
        dto.setInsurance(strDate3);
        DateFormat dateFormat4 = new SimpleDateFormat("MM/dd/yyyy");
        String strDate4 = dateFormat4.format(vehicle.getVignette());
        dto.setVignette(strDate4);
        return dto;
    }

    @Override
    public VehicleDTO findByRegistrationPlate(String registrationPlate) throws NoSuchEntityException {
        Optional<Vehicle> optional = vehicleRepository.findByRegistrationPlate(registrationPlate);
        if (optional.isPresent()) {
            System.out.println(optional.get());
            return createDTO(optional.get());
        } else {
            throw new NoSuchEntityException(registrationPlate);
        }
    }


    @Override
    public List<VehicleDTO> findByRPlate(String registrationPlate) {
        Optional<Vehicle> vehicle = vehicleRepository.findByRegistrationPlate(registrationPlate);
        VehicleDTO dto;
        if (vehicle.isPresent()) {
            dto = createDTO(vehicle.get());
        } else {
            dto = new VehicleDTO();
        }
        List<VehicleDTO> list = new ArrayList<>();
        list.add(dto);
        return list;
    }

    @Override
    public void addVehicle(VehicleDTO vehicle) throws ParseException {
        String sDate1 = vehicle.getOilAndFilters();
        Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate1);
        String sDate2 = vehicle.getTimingBelt();
        Date date2 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate2);
        String sDate3 = vehicle.getInsurance();
        Date date3 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate3);
        String sDate4 = vehicle.getVignette();
        Date date4 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate4);
        Vehicle addingVehicle = new Vehicle(vehicle.getRegistrationPlate(), date1, date2, date3, date4);
        vehicleRepository.save(addingVehicle);
    }

    @Override
    public void updateVehicle(VehicleDTO vehicle) throws NoSuchEntityException, ParseException {
        Optional<Vehicle> optional = vehicleRepository.findById(vehicle.getRegistrationPlate());
        if (optional.isPresent()) {
            Vehicle vehicleEntity = optional.get();
            vehicleEntity.setRegistrationPlate(vehicle.getRegistrationPlate());
            String sDate1 = vehicle.getOilAndFilters();
            Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate1);
            vehicleEntity.setOilAndFilters(date1);
            String sDate2 = vehicle.getTimingBelt();
            Date date2 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate2);
            vehicleEntity.setTimingBelt(date2);
            String sDate3 = vehicle.getInsurance();
            Date date3 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate3);
            vehicleEntity.setInsurance(date3);
            String sDate4 = vehicle.getVignette();
            Date date4 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate4);
            vehicleEntity.setVignette(date4);
            vehicleRepository.save(vehicleEntity);
        } else {
            throw new NoSuchEntityException(String.valueOf(vehicle.getRegistrationPlate()));
        }
    }

    @Override
    public void deleteVehicle(String registrationPlate) {
        vehicleRepository.deleteByRegistrationPlate(registrationPlate);
    }

    @Override
    public List<TimelineDTO> getVehiclesWithToDoDate(int x) {
        List<TimelineDTO> list = new ArrayList<>();
        for (Vehicle v : vehicleRepository.findAll()) {
            Date d1 = v.getOilAndFilters();
            Calendar c1 = Calendar.getInstance();
            c1.setTime(d1);
            c1.add(Calendar.DATE, 365);
            d1 = c1.getTime();
            long dif1 = d1.getTime() - new Timestamp(System.currentTimeMillis()).getTime();
            TimeUnit time1 = TimeUnit.DAYS;
            long differenceInDates1 = time1.convert(dif1, TimeUnit.MILLISECONDS);
            DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
            String strDate1 = dateFormat1.format(d1);
            if (differenceInDates1 < x) {
                list.add(createTimelineDTO(v, "oil and filters", strDate1));
            }
            Date d2 = v.getTimingBelt();
            Calendar c2 = Calendar.getInstance();
            c2.setTime(d2);
            c2.add(Calendar.DATE, 365);
            d2 = c2.getTime();
            long dif2 = d2.getTime() - new Timestamp(System.currentTimeMillis()).getTime();
            TimeUnit time2 = TimeUnit.DAYS;
            long differenceInDates2 = time2.convert(dif2, TimeUnit.MILLISECONDS);
            DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            String strDate2 = dateFormat2.format(d2);
            if (differenceInDates2 < x) {
                list.add(createTimelineDTO(v, "timing belt", strDate2));
            }
            Date d3 = v.getInsurance();
            Calendar c3 = Calendar.getInstance();
            c3.setTime(d3);
            c3.add(Calendar.DATE, 365);
            d3 = c3.getTime();
            long dif3 = d3.getTime() - new Timestamp(System.currentTimeMillis()).getTime();
            TimeUnit time3 = TimeUnit.DAYS;
            long differenceInDates3 = time3.convert(dif3, TimeUnit.MILLISECONDS);
            DateFormat dateFormat3 = new SimpleDateFormat("MM/dd/yyyy");
            String strDate3 = dateFormat3.format(d3);
            if (differenceInDates3 < x) {
                list.add(createTimelineDTO(v, "insurance", strDate3));
            }
            Date d4 = v.getVignette();
            Calendar c4 = Calendar.getInstance();
            c4.setTime(d4);
            c4.add(Calendar.DATE, 365);
            d4 = c4.getTime();
            long dif4 = d4.getTime() - new Timestamp(System.currentTimeMillis()).getTime();
            TimeUnit time4 = TimeUnit.DAYS;
            long differenceInDates4 = time4.convert(dif4, TimeUnit.MILLISECONDS);
            DateFormat dateFormat4 = new SimpleDateFormat("MM/dd/yyyy");
            String strDate4 = dateFormat4.format(d4);
            if (differenceInDates4 < x) {
                list.add(createTimelineDTO(v, "vignette", strDate4));
            }
        }
        return list;
    }

    private TimelineDTO createTimelineDTO(Vehicle v, String problem, String problemType) {
        TimelineDTO dto = new TimelineDTO();
        dto.setName(v.getRegistrationPlate() + " " + problem);
        dto.setDate(problemType);
        return dto;
    }

}
