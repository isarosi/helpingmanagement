package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.dto.VehicleDTO;
import com.example.helpingmanagement.entities.Vehicle;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface VehicleService {

    public List<VehicleDTO> getAllVehicles();

    public VehicleDTO findByRegistrationPlate (String registrationPlate) throws NoSuchEntityException;

    public List<VehicleDTO> findByRPlate(String registrationPlate);

    public void addVehicle (VehicleDTO vehicle) throws ParseException;

    public void updateVehicle (VehicleDTO vehicle) throws NoSuchEntityException, ParseException;

    public void deleteVehicle (String registrationPlate);

    public List<TimelineDTO> getVehiclesWithToDoDate(int x);
}
