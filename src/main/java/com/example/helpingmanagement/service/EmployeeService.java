package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.EmployeeDTO;
import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface EmployeeService {

    public List<EmployeeDTO> getAllEmployees ();

    public EmployeeDTO findById(Long id) throws NoSuchEntityException;

    public List<EmployeeDTO> findByFirstAndLastName(String lastName)throws NoSuchEntityException;

    public void addEmployee (EmployeeDTO employee) throws ParseException;

    public void updateEmployee (EmployeeDTO employee) throws NoSuchEntityException, ParseException;

    public void deleteEmployee (Long id);

    public List<TimelineDTO> getEmployeesWithCloseRaiseDate(int x);
}
