package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.EmployeeDTO;
import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.entities.Employee;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;


    @Override
    public List<EmployeeDTO> getAllEmployees() {
        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeDTO> list = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeDTO dto = createDTO(employee);
            list.add(dto);
        }
        return list;
    }

    private EmployeeDTO createDTO(Employee employee) {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setId(employee.getId());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = dateFormat.format(employee.getBirthDate());
        dto.setBirthDate(strDate);
        dto.setWage(employee.getWage());
        DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
        String strDate2 = dateFormat2.format(employee.getConsiderDateOfRaise());
        dto.setConsiderDateOfRaise(strDate2);
        return dto;
    }

    @Override
    public EmployeeDTO findById(Long id) throws NoSuchEntityException {
        Optional<Employee> optional = employeeRepository.findById(id.intValue());
        if (optional.isPresent()) {
            System.out.println(optional.get());
            return createDTO(optional.get());
        } else {
            throw new NoSuchEntityException(id.toString());
        }
    }

    @Override
    public List<EmployeeDTO> findByFirstAndLastName(String lastName) throws NoSuchEntityException {
        List<Employee> employeeList = employeeRepository.findByFirstName(lastName);
        List<Employee> employeeList2 = employeeRepository.findByLastName(lastName);
        List<EmployeeDTO> list = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeDTO dto = createDTO(employee);
            list.add(dto);
        }
        for (Employee employee : employeeList2) {
            EmployeeDTO dto = createDTO(employee);
            list.add(dto);
        }
        return list;
    }

    @Override
    public void addEmployee(EmployeeDTO employee) throws ParseException {
        String sDate1 = employee.getBirthDate();
        Date date1=new SimpleDateFormat("MM/dd/yyyy").parse(sDate1);
        String sDate2 = employee.getConsiderDateOfRaise();
        Date date2=new SimpleDateFormat("MM/dd/yyyy").parse(sDate2);
        Employee addingEmployee = new Employee(employee.getFirstName(), employee.getLastName(),
                date1, employee.getWage(), date2);
        employeeRepository.save(addingEmployee);
    }

    @Override
    public void updateEmployee(EmployeeDTO employee) throws NoSuchEntityException, ParseException {
        Optional<Employee> optional = employeeRepository.findById(employee.getId());
        if (optional.isPresent()) {
            Employee employeeEntity = optional.get();
            employeeEntity.setFirstName(employee.getFirstName());
            employeeEntity.setLastName(employee.getLastName());
            String sDate1 = employee.getBirthDate();
            Date date1=new SimpleDateFormat("MM/dd/yyyy").parse(sDate1);
            employeeEntity.setBirthDate(date1);
            employeeEntity.setWage(employee.getWage());
            String sDate2 = employee.getConsiderDateOfRaise();
            Date date2=new SimpleDateFormat("MM/dd/yyyy").parse(sDate2);
            employeeEntity.setConsiderDateOfRaise(date2);
            employeeRepository.save(employeeEntity);
        } else {
            throw new NoSuchEntityException(String.valueOf(employee.getId()));
        }
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id.intValue());
    }

    @Override
    public List<TimelineDTO> getEmployeesWithCloseRaiseDate(int x) {
        List<TimelineDTO> list = new ArrayList<>();
        for (Employee e : employeeRepository.findAll()) {
            Date d = e.getConsiderDateOfRaise();
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            c.add(Calendar.DATE, 250);
            d = c.getTime();
            long dif = d.getTime() - new Timestamp(System.currentTimeMillis()).getTime();
            TimeUnit time = TimeUnit.DAYS;
            long differenceInDates = time.convert(dif, TimeUnit.MILLISECONDS);
            if (differenceInDates < x) {
                list.add(createTimelineDTO(e));
            }
        }
        return list;
    }

    private TimelineDTO createTimelineDTO(Employee e) {
        TimelineDTO dto = new TimelineDTO();
        dto.setName(e.getFirstName() + " " + e.getLastName());
        Date d = e.getConsiderDateOfRaise();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.DATE, 250);
        d = c.getTime();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = dateFormat.format(d);
        dto.setDate(strDate);
        return dto;
    }
}
