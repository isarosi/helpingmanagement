package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.MachineTypeDTO;
import com.example.helpingmanagement.entities.MachineType;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.repository.MachineTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MachineTypesServiceImpl implements MachineTypeService {

    @Autowired
    private MachineTypeRepository machineTypeRepository;

    @Override
    public List<MachineTypeDTO> getAllMachineTypes () {
        List<MachineType> machineTypeList = machineTypeRepository.findAll();
        List<MachineTypeDTO> list = new ArrayList<>();
        for (MachineType machineType : machineTypeList) {
            MachineTypeDTO dto = createDTO(machineType);
            list.add(dto);
        }
        return list;
    }

    private MachineTypeDTO createDTO (MachineType machineType) {
        MachineTypeDTO dto = new MachineTypeDTO();
        dto.setId(machineType.getId());
        dto.setType(machineType.getType());
        dto.setRecurrence(machineType.getRecurrence());
        return dto;
    }

    @Override
    public MachineTypeDTO findById(Long id) throws NoSuchEntityException {
        Optional<MachineType> optional = machineTypeRepository.findById(id.intValue());
        if (optional.isPresent()) {
            System.out.println(optional.get());
            return createDTO(optional.get());
        } else {
            throw new NoSuchEntityException(id.toString());
        }
    }

    @Override
    public void addMachineType (MachineTypeDTO machineType) {
        MachineType addingMachineType = new MachineType((machineType.getId()), machineType.getType(), machineType.getRecurrence());
        machineTypeRepository.save(addingMachineType);
    }

    @Override
    public void updateMachineType (MachineTypeDTO machineType) throws NoSuchEntityException {
        Optional<MachineType> optional = machineTypeRepository.findById(machineType.getId());
        if (optional.isPresent()) {
            MachineType machineTypeEntity = optional.get();
            machineTypeEntity.setType(machineType.getType());
            machineTypeEntity.setRecurrence(machineType.getRecurrence());
            machineTypeRepository.save(machineTypeEntity);
        } else {
            throw new NoSuchEntityException(String.valueOf(machineType.getId()));
        }
    }

    @Override
    public void deleteMachineType (Long id) {
        machineTypeRepository.deleteById(id.intValue());
    }
}
