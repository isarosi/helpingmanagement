package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.EmployeeDTO;
import com.example.helpingmanagement.dto.FactoryMachineDTO;
import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.entities.FactoryMachine;
import com.example.helpingmanagement.entities.MachineType;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import com.example.helpingmanagement.repository.FactoryMachineRepository;
import com.example.helpingmanagement.repository.MachineTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class FactoryMachineServiceImpl implements FactoryMachineService {

    @Autowired
    private FactoryMachineRepository factoryMachineRepository;

    @Autowired
    private MachineTypeRepository machineTypeRepository;

    @Override
    public List<FactoryMachineDTO> getAllFactoryMachines() {
        List<FactoryMachine> factoryMachineList = factoryMachineRepository.findAll();
        List<FactoryMachineDTO> list = new ArrayList<>();
        for (FactoryMachine factoryMachine : factoryMachineList) {
            FactoryMachineDTO dto = createDTO(factoryMachine);
            list.add(dto);
        }
        return list;
    }

    private FactoryMachineDTO createDTO(FactoryMachine factoryMachine) {
        FactoryMachineDTO dto = new FactoryMachineDTO();
        dto.setId(factoryMachine.getId());
        dto.setMachineType(factoryMachine.getMachineType().getType());
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = dateFormat.format(factoryMachine.getLastMaintenance());
        dto.setLastMaintenance(strDate);
        return dto;
    }

    @Override
    public FactoryMachineDTO findById(Long id) throws NoSuchEntityException {
        Optional<FactoryMachine> optional = factoryMachineRepository.findById(id.intValue());
        if (optional.isPresent()) {
            System.out.println(optional.get());
            return createDTO(optional.get());
        } else {
            throw new NoSuchEntityException(id.toString());
        }

    }

    @Override
    public List<FactoryMachineDTO> searchByAnId(Integer id){
        List<FactoryMachine> factoryMachineList = factoryMachineRepository.searchById(id);
        List<FactoryMachineDTO> list = new ArrayList<>();
        for (FactoryMachine factoryMachine : factoryMachineList) {
            FactoryMachineDTO dto = createDTO(factoryMachine);
            list.add(dto);
        }
        return list;
    }

    @Override
    public void addFactoryMachine(FactoryMachineDTO factoryMachine) throws ParseException {
        Optional<MachineType> type = machineTypeRepository.findByType(factoryMachine.getMachineType());
        String sDate = factoryMachine.getLastMaintenance();
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(sDate);
        FactoryMachine addingFactoryMachine = new FactoryMachine(type.isPresent()? type.get() : null, date);
        factoryMachineRepository.save(addingFactoryMachine);
    }

    @Override
    public void updateFactoryMachine(FactoryMachineDTO factoryMachine) throws NoSuchEntityException, ParseException {
        Optional<FactoryMachine> optional = factoryMachineRepository.findById(factoryMachine.getId());
        if (optional.isPresent()) {
            FactoryMachine factoryMachineEntity = optional.get();
            Optional<MachineType> type = machineTypeRepository.findByType(factoryMachine.getMachineType());
            factoryMachineEntity.setMachineType(type.isPresent()? type.get() : null);
            String sDate = factoryMachine.getLastMaintenance();
            Date date = new SimpleDateFormat("MM/dd/yyyy").parse(sDate);
            factoryMachineEntity.setLastMaintenance(date);
            factoryMachineRepository.save(factoryMachineEntity);
        } else {
            throw new NoSuchEntityException(String.valueOf(factoryMachine.getId()));
        }
    }

    @Override
    public void deleteFactoryMachine(Long id) {
        factoryMachineRepository.deleteById(id.intValue());
    }

    @Override
    public List<TimelineDTO> getFactoryMachinesWithToDoDate(int x) {
        List<TimelineDTO> list = new ArrayList<>();
        for (FactoryMachine f : factoryMachineRepository.findAll()) {
            Date d = f.getLastMaintenance();
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            c.add(Calendar.DATE, f.getMachineType().getRecurrence());
            d = c.getTime();
            long dif = d.getTime() - new Timestamp(System.currentTimeMillis()).getTime();
            TimeUnit time = TimeUnit.DAYS;
            long differenceInDates = time.convert(dif, TimeUnit.MILLISECONDS);
            if (differenceInDates < x) {
                list.add(createTimelineDTO(f));
            }
        }
        return list;
    }

    private TimelineDTO createTimelineDTO(FactoryMachine f) {
        TimelineDTO dto = new TimelineDTO();
        dto.setName(f.getId() + " " + f.getMachineType().getType());
        Date d = f.getLastMaintenance();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.DATE, f.getMachineType().getRecurrence());
        d = c.getTime();
        DateFormat dateFormat4 = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = dateFormat4.format(d);
        dto.setDate(strDate);
        return dto;
    }
}
