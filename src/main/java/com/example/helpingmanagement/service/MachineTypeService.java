package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.FactoryMachineDTO;
import com.example.helpingmanagement.dto.MachineTypeDTO;
import com.example.helpingmanagement.entities.MachineType;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MachineTypeService {

    public List<MachineTypeDTO> getAllMachineTypes();

    public MachineTypeDTO findById (Long id) throws NoSuchEntityException;

    public void addMachineType (MachineTypeDTO machineType);

    public void updateMachineType (MachineTypeDTO machineType) throws NoSuchEntityException;

    public void deleteMachineType (Long id);
}
