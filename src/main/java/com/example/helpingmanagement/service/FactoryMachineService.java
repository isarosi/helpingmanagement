package com.example.helpingmanagement.service;

import com.example.helpingmanagement.dto.FactoryMachineDTO;
import com.example.helpingmanagement.dto.TimelineDTO;
import com.example.helpingmanagement.entities.FactoryMachine;
import com.example.helpingmanagement.entities.MachineType;
import com.example.helpingmanagement.exceptions.NoSuchEntityException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface FactoryMachineService {

    public List<FactoryMachineDTO> getAllFactoryMachines();

    public FactoryMachineDTO findById(Long id) throws NoSuchEntityException;

    public List<FactoryMachineDTO> searchByAnId(Integer id);

    public void addFactoryMachine (FactoryMachineDTO factoryMachine) throws ParseException;

    public void updateFactoryMachine (FactoryMachineDTO factoryMachine) throws NoSuchEntityException, ParseException;

    public void deleteFactoryMachine (Long id);

    public List<TimelineDTO> getFactoryMachinesWithToDoDate(int x);
}
