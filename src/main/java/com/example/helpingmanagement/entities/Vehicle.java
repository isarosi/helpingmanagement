package com.example.helpingmanagement.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @Column(name = "registration_plate")
    private String registrationPlate;

    @Column(name = "oil_filters")
    private Date oilAndFilters;

    @Column(name = "timing_belt")
    private Date timingBelt;

    @Column(name = "insurance")
    private Date insurance;

    @Column(name = "vignette")
    private Date vignette;

    public Vehicle() {
    }

    public Vehicle(String registrationPlate, Date oilAndFilters, Date timingBelt, Date insurance, Date vignette) {
        this.registrationPlate = registrationPlate;
        this.oilAndFilters = oilAndFilters;
        this.timingBelt = timingBelt;
        this.insurance = insurance;
        this.vignette = vignette;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public void setRegistrationPlate(String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    public Date getOilAndFilters() {
        return oilAndFilters;
    }

    public void setOilAndFilters(Date oilAndFilters) {
        this.oilAndFilters = oilAndFilters;
    }

    public Date getTimingBelt() {
        return timingBelt;
    }

    public void setTimingBelt(Date timingBelt) {
        this.timingBelt = timingBelt;
    }

    public Date getInsurance() {
        return insurance;
    }

    public void setInsurance(Date insurance) {
        this.insurance = insurance;
    }

    public Date getVignette() {
        return vignette;
    }

    public void setVignette(Date vignette) {
        this.vignette = vignette;
    }
}
