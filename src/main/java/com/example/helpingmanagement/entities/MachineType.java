package com.example.helpingmanagement.entities;

import javax.persistence.*;

@Entity
@Table(name = "machine_types")
public class MachineType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "type")
    private String type;

    @Column(name = "recurrence")
    private int recurrence;

    public MachineType() {
    }

    public MachineType(Integer id, String type, int recurrence) {
        this.id = id;
        this.type = type;
        this.recurrence = recurrence;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int recurrence) {
        this.recurrence = recurrence;
    }
}
