package com.example.helpingmanagement.entities;

import javax.persistence.*;
import java.util.Date;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name", length = 45)
    private String firstName;

    @Column(name = "last_name", length = 45)
    private String lastName;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "wage")
    private int wage;

    @Column(name = "consider_raise")
    private Date considerDateOfRaise;

    public Employee() {
    }

    public Employee(String firstName, String lastName, Date birthDate, int wage, Date considerDateOfRaise) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.wage = wage;
        this.considerDateOfRaise = considerDateOfRaise;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getWage() {
        return wage;
    }

    public void setWage(int wage) {
        this.wage = wage;
    }

    public Date getConsiderDateOfRaise() {
        return considerDateOfRaise;
    }

    public void setConsiderDateOfRaise(Date considerDateOfRaise) {
        this.considerDateOfRaise = considerDateOfRaise;
    }
}
