package com.example.helpingmanagement.dto;

public class MachineTypeDTO {

    private Integer id;
    private String type;
    private int recurrence;

    public MachineTypeDTO() {
    }

    public MachineTypeDTO(Integer id, String type, int recurrence) {
        this.id = id;
        this.type = type;
        this.recurrence = recurrence;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int recurrence) {
        this.recurrence = recurrence;
    }
}
