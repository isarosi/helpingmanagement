package com.example.helpingmanagement.dto;

import java.util.Date;

public class VehicleDTO {

    private String registrationPlate;
    private String oilAndFilters;
    private String timingBelt;
    private String insurance;
    private String vignette;

    public VehicleDTO() {
    }

    public VehicleDTO(String registrationPlate, String oilAndFilters,
                      String timingBelt, String insurance, String vignette) {
        this.registrationPlate = registrationPlate;
        this.oilAndFilters = oilAndFilters;
        this.timingBelt = timingBelt;
        this.insurance = insurance;
        this.vignette = vignette;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public void setRegistrationPlate(String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    public String getOilAndFilters() {
        return oilAndFilters;
    }

    public void setOilAndFilters(String oilAndFilters) {
        this.oilAndFilters = oilAndFilters;
    }

    public String getTimingBelt() {
        return timingBelt;
    }

    public void setTimingBelt(String timingBelt) {
        this.timingBelt = timingBelt;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getVignette() {
        return vignette;
    }

    public void setVignette(String vignette) {
        this.vignette = vignette;
    }
}
