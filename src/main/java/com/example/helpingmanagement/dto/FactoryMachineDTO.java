package com.example.helpingmanagement.dto;

import com.example.helpingmanagement.entities.MachineType;

import java.util.Date;

public class FactoryMachineDTO {

    private Integer id;
    private String machineType;
    private String lastMaintenance;

    public FactoryMachineDTO() {
    }

    public FactoryMachineDTO(Integer id, String machineType, String lastMaintenance) {
        this.id = id;
        this.machineType = machineType;
        this.lastMaintenance = lastMaintenance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getLastMaintenance() {
        return lastMaintenance;
    }

    public void setLastMaintenance(String lastMaintenance) {
        this.lastMaintenance = lastMaintenance;
    }
}
