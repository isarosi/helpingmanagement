package com.example.helpingmanagement.dto;

import java.util.Date;

public class EmployeeDTO {

    private int id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private int wage;
    private String considerDateOfRaise;

    public EmployeeDTO() {
    }

    public EmployeeDTO(int id, String firstName, String lastName, String birthDate, int wage, String considerDateOfRaise) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.wage = wage;
        this.considerDateOfRaise = considerDateOfRaise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getWage() {
        return wage;
    }

    public void setWage(int wage) {
        this.wage = wage;
    }

    public String getConsiderDateOfRaise() {
        return considerDateOfRaise;
    }

    public void setConsiderDateOfRaise(String considerDateOfRaise) {
        this.considerDateOfRaise = considerDateOfRaise;
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", wage=" + wage +
                ", considerDateOfRaise=" + considerDateOfRaise +
                '}';
    }
}
