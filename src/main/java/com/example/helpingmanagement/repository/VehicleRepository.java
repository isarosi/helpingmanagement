package com.example.helpingmanagement.repository;

import com.example.helpingmanagement.dto.VehicleDTO;
import com.example.helpingmanagement.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, String> {

    Optional<Vehicle> findByRegistrationPlate (String registrationPlate);
    List<Vehicle> deleteByRegistrationPlate (String registrationPlate);
}

