package com.example.helpingmanagement.repository;

import com.example.helpingmanagement.entities.MachineType;
import com.example.helpingmanagement.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MachineTypeRepository extends JpaRepository<MachineType, Integer> {

    Optional<MachineType> findByType(String registrationPlate);
}
