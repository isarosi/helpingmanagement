package com.example.helpingmanagement.repository;

import com.example.helpingmanagement.dto.EmployeeDTO;
import com.example.helpingmanagement.entities.Employee;
import com.example.helpingmanagement.entities.FactoryMachine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    List<Employee> findByLastName(String lastName);
    List<Employee> findByFirstName(String lastName);



}
