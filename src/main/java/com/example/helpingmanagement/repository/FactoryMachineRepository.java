package com.example.helpingmanagement.repository;

import com.example.helpingmanagement.entities.FactoryMachine;
import com.example.helpingmanagement.entities.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FactoryMachineRepository extends JpaRepository<FactoryMachine, Integer> {

    List<FactoryMachine> searchById(Integer id);
}

